package com.notif.app;

import android.content.Context;

import androidx.multidex.MultiDexApplication;

import com.google.firebase.FirebaseApp;
import com.google.firebase.messaging.FirebaseMessaging;

import timber.log.Timber;

public class BaseApp extends MultiDexApplication {
    public static Context context;
    @Override
    public void onCreate() {
        super.onCreate();
        //MultiDex.install(this);
        context = getApplicationContext();

        Timber.plant(new Timber.DebugTree());

        try {
            FirebaseApp.initializeApp(this);
            FirebaseMessaging.getInstance().setAutoInitEnabled(true);
        } catch (RuntimeException e) {
            Timber.e(e);
        }
        //FirebaseCrashlytics.getInstance().setCrashlyticsCollectionEnabled(true);

        //BehtifConfig.getInstance(this);
    }
}
