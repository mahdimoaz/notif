package com.notif.behtif;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class NotifReminder implements Serializable {
    @SerializedName("notif_type_id")
    int notif_type_id;
    @SerializedName("notif_type_fa")
    String notif_type_fa;
    @SerializedName("action")
    boolean action;

    public void setNotif_type_fa(String notif_type_fa) {
        this.notif_type_fa = notif_type_fa;
    }

    public String getNotif_type_fa() {
        return notif_type_fa;
    }

    public void setAction(boolean action) {
        this.action = action;
    }

    public void setNotif_type_id(int notif_type_id) {
        this.notif_type_id = notif_type_id;
    }

    public int getNotif_type_id() {
        return notif_type_id;
    }

    public boolean isAction() {
        return action;
    }
}
