package com.notif.behtif;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.PowerManager;

import androidx.core.app.NotificationCompat;

import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;


import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Random;

import timber.log.Timber;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    /* public static void start(final Context context) {
     *//* Intent startServiceIntent = new Intent(context, MyFirebaseMessagingService.class);
        context.startService(startServiceIntent);*//*
        final Intent notificationServiceIntent = new Intent(context, MyFirebaseMessagingService.class);

        G.getComponent().getHandler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    context.startForegroundService(notificationServiceIntent);
                } else {
                    context.startService(notificationServiceIntent);
                }
            }
        }, 5000);


    }*/

    private static final String TAG = "MyFirebaseMsgService";

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        scheduleJob(remoteMessage);
        // Timber.e(new Gson().toJson(remoteMessage.getData()));
        // super.onMessageReceived(remoteMessage);
    }

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    @Override
    public void onNewToken(String token) {
        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
    }
    // [END on_new_token]

    private void scheduleJob(RemoteMessage remoteMessage) {
        // [START dispatch_job]
        FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(this));
        Job myJob = dispatcher.newJobBuilder()
                .setService(MyJobService.class)
                .setTag("my-job-tag")
                .build();
        dispatcher.schedule(myJob);
        handleNow(remoteMessage);
        // [END dispatch_job]
    }

    /**
     * Handle time allotted to BroadcastReceivers.
     */
    private void handleNow(RemoteMessage remoteMessage) {

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            // Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
            //sendNotification(remoteMessage.getNotification());
        } else {
           // Timber.e(new Gson().toJson(remoteMessage.getData()));
            sendNotification(new Gson().fromJson(new Gson().toJson(remoteMessage.getData()), NotifModel.class));
        }
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */


    Uri defaultSoundUri;
    Bitmap myBitmap;
    Bitmap largeIcon;
    NotificationCompat.BigPictureStyle bigPictureStyle;
    NotificationCompat.Builder notificationBuilder;
    PendingIntent pendingIntent;
    String channelId = "Behtif";
    Intent intent;
    boolean canceled = true;

    private void sendNotification(final NotifModel messageBody) {

       /* if (messageBody.getLinkUrl() != null)
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse(messageBody.getLinkUrl()));
        else {
            intent = new Intent(this, ActivitySplash.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
        }*/
        canceled = messageBody.isAutoCancel();
        channelId = messageBody.getChannel_id();

        if (messageBody.isVisible()) {
            if (messageBody.getAction_type() == NotifModel.actionType.OpenApp) {
                intent = new Intent(messageBody.getPackageName());
                intent.setPackage(messageBody.getPackageName());
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
            } else if (messageBody.getAction_type() == NotifModel.actionType.OpenEspecialApp) {
                intent = getPackageManager().getLaunchIntentForPackage(messageBody.getAction_page());
                if (intent == null) {
                    intent = new Intent(messageBody.getAction_page());
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
                }
            } else if (messageBody.getAction_type() == NotifModel.actionType.OpenPage) {

                intent = new Intent(messageBody.getAction_page());
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

            } else if (messageBody.getAction_type() == NotifModel.actionType.OpenTelegramChannal) {
                try {
                    if (NotifUtils.isAppAvailable(getApplicationContext(), "org.telegram.messenger")) {
                        intent = new Intent(Intent.ACTION_VIEW);
                        intent.setPackage("org.telegram.messenger");
                        intent.setData(Uri.parse(messageBody.getAction_page()));
                    } else {
                        intent = new Intent(Intent.ACTION_VIEW, Uri.parse(messageBody.getAction_page()));
                    }
                } catch (Exception e) {
                    intent = new Intent(Intent.ACTION_VIEW, Uri.parse(messageBody.getAction_page()));
                }
            } else if (messageBody.getAction_type() == NotifModel.actionType.OpenInstagramPage) {
                if (NotifUtils.isAppAvailable(getApplicationContext(), "com.instagram.android")) {
                    intent = new Intent(Intent.ACTION_VIEW);
                    intent.setPackage("com.instagram.android");
                    intent.setData(Uri.parse(messageBody.getAction_page()));

                } else {
                    intent = new Intent(Intent.ACTION_VIEW, Uri.parse(messageBody.getAction_page()));
                }
            } else if (messageBody.getAction_type() == NotifModel.actionType.OpenWebUrl) {
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse(messageBody.getAction_page()));
            }

            pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                    PendingIntent.FLAG_ONE_SHOT);
            if (messageBody.getIcon() != null) {
                try {
                    URL url_value = new URL(messageBody.getIcon());
                    largeIcon = BitmapFactory.decodeStream(url_value.openConnection().getInputStream());
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                    largeIcon = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                            R.mipmap.ic_launcher);
                } catch (IOException e) {
                    e.printStackTrace();
                    largeIcon = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                            R.mipmap.ic_launcher);
                }
            } else {
                largeIcon = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                        R.mipmap.ic_launcher);
            }
            defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

            setNotif(messageBody.getTitle(), messageBody.getBody());
            if (messageBody.getImage() != null) {
                try {
                    URL url_value = new URL(messageBody.getImage());
                    myBitmap = BitmapFactory.decodeStream(url_value.openConnection().getInputStream());
                    bigPictureStyle = new NotificationCompat.BigPictureStyle()
                            // Provides the bitmap for the BigPicture notification.
                            .bigPicture(myBitmap)
                            // Overrides ContentTitle in the big form of the template.
                            .setBigContentTitle(messageBody.getTitle())
                            // Summary line after the detail section in the big form of the template.
                            .setSummaryText(messageBody.getBody());

                    setNotifImage(messageBody.getTitle(), messageBody.getBody());
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (messageBody.getActions().size() > 0) {
                for (NotifModel.ActionButton actionb : messageBody.getActions()) {
                    PendingIntent pendingIntent = null;
                    if (actionb.getAction_type() == NotifModel.actionType.OpenApp) {
                        Intent nIntent = new Intent(messageBody.getPackageName());
                        nIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
                        pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, nIntent,
                                PendingIntent.FLAG_ONE_SHOT);
                    } else if (actionb.getAction_type() == NotifModel.actionType.OpenEspecialApp) {
                        Intent nIntent = getPackageManager().getLaunchIntentForPackage(actionb.getAction());
                        if (nIntent == null) {
                            nIntent = new Intent(actionb.getAction());
                            nIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
                        }
                        pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, nIntent,
                                PendingIntent.FLAG_ONE_SHOT);
                    } else if (actionb.getAction_type() == NotifModel.actionType.OpenPage) {

                        Intent nIntent = new Intent(actionb.getAction());
                        nIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
                        nIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, nIntent,
                                PendingIntent.FLAG_ONE_SHOT);
                    } else if (actionb.getAction_type() == NotifModel.actionType.OpenTelegramChannal) {
                        Intent nIntent = null;
                        try {

                            if (NotifUtils.isAppAvailable(getApplicationContext(), "org.telegram.messenger")) {
                                nIntent = new Intent(Intent.ACTION_VIEW);
                                nIntent.setPackage("org.telegram.messenger");
                                nIntent.setData(Uri.parse(actionb.getAction()));
                            } else {
                                nIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(messageBody.getAction_page()));
                            }

                        } catch (Exception e) {
                            nIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(messageBody.getAction_page()));
                        }
                        pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, nIntent,
                                PendingIntent.FLAG_ONE_SHOT);
                    } else if (actionb.getAction_type() == NotifModel.actionType.OpenInstagramPage) {
                        Intent nIntent = null;
                        if (NotifUtils.isAppAvailable(getApplicationContext(), "com.instagram.android")) {
                            nIntent = new Intent(Intent.ACTION_VIEW);
                            nIntent.setPackage("com.instagram.android");
                            nIntent.setData(Uri.parse(actionb.getAction()));

                        } else {
                            nIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(actionb.getAction()));
                        }
                        pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, nIntent,
                                PendingIntent.FLAG_ONE_SHOT);
                    } else if (actionb.getAction_type() == NotifModel.actionType.OpenWebUrl) {
                        Intent nIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(actionb.getAction()));
                        pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, nIntent,
                                PendingIntent.FLAG_ONE_SHOT);
                    }


                    NotificationCompat.Action action = new NotificationCompat.Action(0, actionb.getTitle(), pendingIntent);
                    setNotifButton(messageBody.getTitle(), messageBody.getBody(), action);
                }
                //NotificationCompat.Action action = new NotificationCompat.Action(0, messageBody.getText_bottom(), pendingIntent);

            }

            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationChannel channel = new NotificationChannel(channelId,
                        "اپلیکیشن زیره",
                        NotificationManager.IMPORTANCE_DEFAULT);
                channel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
                notificationManager.createNotificationChannel(channel);
            }
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                notificationBuilder.setColor(Color.BLUE);
            }

            wakeUpScreen();
            notificationManager.notify(String.valueOf(System.currentTimeMillis()), new Random().nextInt(50000), notificationBuilder.build());
        } else {
            EventBus.getDefault().post(messageBody);
        }
    }

    private void setNotif(String title, String body) {
        NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
        bigTextStyle.bigText(body);
        notificationBuilder =
                new NotificationCompat.Builder(getApplicationContext(), channelId)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setLargeIcon(largeIcon)
                        .setContentTitle(title)
                        .setContentText(body)
                        .setStyle(bigTextStyle)
                        .setAutoCancel(canceled)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);
    }

    private void setNotifButton(String title, String body, NotificationCompat.Action action) {
        if (notificationBuilder == null)
            notificationBuilder =
                    new NotificationCompat.Builder(getApplicationContext(), channelId)
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setLargeIcon(largeIcon)
                            .setContentTitle(title)
                            .setContentText(body)
                            .setAutoCancel(true)
                            .setSound(defaultSoundUri)
                            .addAction(action)
                            .setContentIntent(pendingIntent);
        else {
            notificationBuilder.addAction(action);
        }
    }

    private void setNotifImage(String title, String body) {
        if (notificationBuilder != null) {
            notificationBuilder.setStyle(bigPictureStyle);
        } else
            notificationBuilder =
                    new NotificationCompat.Builder(getApplicationContext(), channelId)
                            .setStyle(bigPictureStyle)
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setLargeIcon(largeIcon)
                            .setContentTitle(title)
                            .setContentText(body)
                            .setAutoCancel(canceled)
                            .setSound(defaultSoundUri)
                            .setContentIntent(pendingIntent);
    }

    private void wakeUpScreen() {
        PowerManager pm = (PowerManager) this.getSystemService(POWER_SERVICE);
        boolean isScreenOn = pm.isScreenOn();

        if (!isScreenOn) {
            PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP | PowerManager.ON_AFTER_RELEASE, "myapp:MyLock");
            wl.acquire(1000);
            PowerManager.WakeLock wl_cpu = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "myapp:MyCpuLock");
            wl_cpu.acquire(1000);
        }
    }
}
