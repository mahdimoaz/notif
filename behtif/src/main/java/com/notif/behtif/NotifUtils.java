package com.notif.behtif;


import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import timber.log.Timber;

public class NotifUtils {

    public static boolean isAppAvailable(Context context, String appName) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(appName, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public static boolean isOnline(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        //CheckSpeedLow(context);
        //return (info != null && info.isConnected() && Connectivity.isConnectionFast(info.getType(),info.getSubtype()));
        return netInfo != null && netInfo.isConnectedOrConnecting();//&& isConnectionFast(netInfo.getType(),netInfo.getSubtype())
    }

    public static boolean isInternetAvailable() {
        Runtime runtime = Runtime.getRuntime();
        try {
            Process ipProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8");
            int exitValue = ipProcess.waitFor();
            return (exitValue == 0);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return false;

    }

    public static String getString(int id, Context context) {
        return context.getText(id).toString();
    }
    public static String arabicToDecimal(String number) {
        String arabic = "\u06f0\u06f1\u06f2\u06f3\u06f4\u06f5\u06f6\u06f7\u06f8\u06f9";
        char[] chars = new char[number.length()];
        for (int i = 0; i < number.length(); i++) {
            char ch = number.charAt(i);
            if (ch >= 0x0660 && ch <= 0x0669)
                ch -= 0x0660 - '0';
            else if (ch >= 0x06f0 && ch <= 0x06F9)
                ch -= 0x06f0 - '0';
            chars[i] = ch;
        }
        return new String(chars);

    }

 /*   public static int getAppVersion() {
        try {
            PackageInfo pInfo = G.context.getPackageManager().getPackageInfo(G.context.getPackageName(), 0);
            int version = Integer.parseInt(pInfo.versionName.replace(".", ""));
            return version;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return Integer.parseInt(BuildConfig.VERSION_NAME.replace(".", ""));
        }

    }

    public static String getAppVersionString() {
        try {
            PackageInfo pInfo = G.context.getPackageManager().getPackageInfo(G.context.getPackageName(), 0);
            return pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return BuildConfig.VERSION_NAME;
        }

    }*/



    public static String checkDigit(int number) {
        return number <= 9 ? "0" + number : String.valueOf(number);
    }

    public static String getJwt() {
        return null;
    }

    public static String getMCrypt() {
        DateFormat sdf = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
        try {
            /*UserModel userModel = new UserModel();
            userModel.setAppVersion(Utils.getAppVersion());
            Date d = new Date();
            //userModel.setTime(Sharedpreferenced.getInstance().getString(Sharedpreferenced.SharedPrefsTypes.ToDay) + " " + sdf.format(d));
            userModel.setTime(time);
            return AesCipher.encrypt(getString(R.string.secret, G.context), new Gson().toJson(userModel)).toString();*/
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            return "asda1dad313gjggjrtyr";
        }
        return "asda1dad313gjggjrtyr";
    }


    public static void resetInstanceId() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    FirebaseInstanceId.getInstance().deleteInstanceId();
                } catch (IOException e) {
                    Timber.e(e);
                } catch (RuntimeException e) {
                    Timber.e(e);
                }
            }
        }).start();

    }


    private static boolean isCallable(Context context, Intent intent) {
        List<ResolveInfo> list = context.getPackageManager().queryIntentActivities(intent,
                PackageManager.MATCH_DEFAULT_ONLY);
        return list.size() > 0;
    }




}
