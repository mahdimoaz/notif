package com.notif.behtif;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class NotifModel implements Serializable {

    public enum actionType implements Serializable {
        @SerializedName("0")
        OpenApp("باز کردن اپلیکیشن زیره"),
        @SerializedName("1")
        OpenWebUrl("بازکردن صفحه ی وب"),
        @SerializedName("2")
        OpenEspecialApp("بازکردن اپلیکیشن های دیگر"),
        @SerializedName("3")
        OpenPage("بازکردن صفحه ای از اپلیکیشن"),
        @SerializedName("4")
        OpenTelegramChannal("باز کردن تلگرام"),
        @SerializedName("5")
        OpenInstagramPage("بازکردن اینستاگرام");
        private final String caption;

        actionType(String s) {
            caption = s;
        }

        public String getCaption() {
            return caption;
        }
    }

    @SerializedName("image")
    String image;

    @SerializedName("title")
    String title;
    @SerializedName("description")
    String body;
    @SerializedName("icon")
    String icon;
    @SerializedName("visible")
    String visible;
    @SerializedName("action_type")
    String action_type;
    @SerializedName("channel_id")
    String channel_id;
    @SerializedName("package")
    String packageName;
    @SerializedName("action")
    String action;

    @SerializedName("action_page_app")
    String action_page_app;

    @SerializedName("actions")
    String actions;
    @SerializedName("auto_cancle")
    String auto_cancle;

    public class ActionButton {
        @SerializedName("title")
        String title;
        @SerializedName("action")
        String action;
        @SerializedName("action_type")
        String action_type;

        public void setAction(String action) {
            this.action = action;
        }

        public String getAction() {
            return action;
        }

        public actionType getAction_type() {
            if (actionType.values().length > Integer.parseInt(action_type))
                return actionType.values()[Integer.parseInt(action_type)];
            else return actionType.values()[1];
        }

        public String getTitle() {
            return title;
        }

        public void setAction_type(String action_type) {
            this.action_type = action_type;
        }

        public void setTitle(String title) {
            this.title = title;
        }

    }

    public void setAction_page_app(String action_page_app) {
        this.action_page_app = action_page_app;
    }

    public String getPackageName() {
        return packageName;
    }

    public String getAction_page_app() {
        return action_page_app;
    }

    public String getChannel_id() {
        return channel_id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getTitle() {
        return title;
    }

    public String getBody() {
        return body;
    }


    public void setAction_page(String action_page) {
        this.action = action_page;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }


    public String getImage() {
        return image;
    }

    public String getAction_page() {
        return action;
    }

    public String getIcon() {
        return icon;
    }

    public void setVisible(String visible) {
        this.visible = visible;
    }

    public boolean isVisible() {
        return Boolean.parseBoolean(visible);
    }


    public List<ActionButton> getActions() {
        try {
            return new Gson().fromJson(actions, new TypeToken<List<ActionButton>>() {
            }.getType());
        } catch (Exception e) {
            return new ArrayList<>();
        }

    }
    public void setAction_type(String action_type) {
        this.action_type = action_type;
    }

    public actionType getAction_type() {
        if (actionType.values().length > Integer.parseInt(action_type))
            return actionType.values()[Integer.parseInt(action_type)];
        else return actionType.values()[1];
    }

    public boolean isAutoCancel() {
        return Integer.parseInt(auto_cancle) == 1;
    }
}

